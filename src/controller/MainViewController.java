package controller;

import ch.interlis.iom.IomObject;
import ch.interlis.iox.IoxException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.logging.Level;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import logic.Data2GeoJSON;
import logic.GeometryTests;
import logic.ReadInterlis;
import main.MainApp;
import tools.Logger;

/**
 * FXML Controller class for the MainView
 *
 * @author schlatter
 */
public class MainViewController {

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private TextField txtInterlis;

    @FXML
    private TextField txtLog;

    @FXML
    private TextField txtMap;

    @FXML
    private TextField txtNumTestObjects;

    @FXML
    private TextField txtCoordsX;

    @FXML
    private TextField txtCoordsY;

    @FXML
    private Button btnChooseInterlis;

    @FXML
    private Button btnReadInterlis;

    @FXML
    private Button btnChooseLog;

    @FXML
    private Button btnChooseMap;

    @FXML
    private CheckBox chkBoxLine;

    @FXML
    private CheckBox chkBoxPoly;

    @FXML
    private CheckBox chkBoxArc;

    @FXML
    private Label lblStatus;

    @FXML
    private Button btnMap;

    @FXML
    private Button btnStart;

    private ReadInterlis readInterlis = new ReadInterlis();
    private MainApp mainApp;
    private static final String SLASH = File.separator;
    private Map<String, IomObject> polyMap = new HashMap<>();
    private Map<String, IomObject> polyWArcMap = new HashMap<>();
    private final static java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(java.util.logging.Logger.GLOBAL_LOGGER_NAME);
    private Timeline timeline;
    private final int maxLength = 20;

    /**
     * is automatically used when it gets opened together with the view
     */
    @FXML
    private void initialize() {
        this.loadProperties();

        // force the textfield to be numeric only
        this.txtNumTestObjects.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*")) {
                    lblStatus.setText("Es sind nur ganze Zahlen erlaubt");
                    txtNumTestObjects.setText(newValue.replaceAll("[^\\d]", ""));
                }
                //maxLength of textField 
                if (txtNumTestObjects.getText().length() > maxLength) {
                    String s = txtNumTestObjects.getText().substring(0, maxLength);
                    txtNumTestObjects.setText(s);
                }
            }
        });
        //observes txtNumTestObjects - only if its 1 it is allowed to use the coordinates textfields
        this.txtNumTestObjects.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.equals("1")) {
                this.txtCoordsX.setEditable(true);
                this.txtCoordsY.setEditable(true);
            } else {
                this.txtCoordsX.setText("");
                this.txtCoordsY.setText("");
                this.txtCoordsX.setEditable(false);
                this.txtCoordsY.setEditable(false);
            }
        });
        // force the textfield to be numeric only
        this.txtCoordsX.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*")) {
                    lblStatus.setText("Es sind nur ganze Zahlen erlaubt");
                    txtCoordsX.setText(newValue.replaceAll("[^\\d]", ""));
                }
                //maxLength of textField 
                if (txtCoordsX.getText().length() > maxLength) {
                    String s = txtCoordsX.getText().substring(0, maxLength);
                    txtCoordsX.setText(s);
                }
            }
        });
        // force the textfield to be numeric only
        this.txtCoordsY.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*")) {
                    lblStatus.setText("Es sind nur ganze Zahlen erlaubt");
                    txtCoordsY.setText(newValue.replaceAll("[^\\d]", ""));
                }
                //maxLength of textField 
                if (txtCoordsY.getText().length() > maxLength) {
                    String s = txtCoordsY.getText().substring(0, maxLength);
                    txtCoordsY.setText(s);
                }
            }
        });
    }

    /**
     * Checks which button used this method and uses the openChooser method
     *
     * @param event
     */
    @FXML
    private void checkBtnClicked(MouseEvent event) {
        if (event.getSource().toString().contains("btnChooseInterlis")) {
            this.openChooser(this.txtInterlis, false);
        } else if (event.getSource().toString().contains("btnChooseLog")) {
            this.openChooser(this.txtLog, true);
        } else if (event.getSource().toString().contains("btnChooseMap")) {
            this.openChooser(this.txtMap, true);
        }
    }

    /**
     * Opens directory - or fileChooser
     *
     * @param textField - sets for the given textField the path
     * @param directory - boolean to choose if it should open the file - or the
     * directoryChooser
     */
    @FXML
    private void openChooser(TextField textField, boolean directory) {
        //check if directory or file
        if (directory == true) {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            directoryChooser.setTitle("Verzeichnis auswählen");

            //displays the open dialog on current Window
            File selectedFile = directoryChooser.showDialog(this.anchorPane.getScene().getWindow());

            //checks if something's chosen
            if (selectedFile != null) {
                textField.setText(selectedFile.getAbsolutePath());
            }

        } else {
            FileChooser fileChooser = new FileChooser();
            //adds extension-filter
            FileChooser.ExtensionFilter extFilterXTF = new FileChooser.ExtensionFilter("INTERLIS 2 Dateien (*.xtf)", "*.xtf");
            fileChooser.getExtensionFilters().add(extFilterXTF);

            fileChooser.setTitle("Datei auswählen");
            File selectedFile = fileChooser.showOpenDialog(this.anchorPane.getScene().getWindow());

            if (selectedFile != null) {
                textField.setText(selectedFile.getAbsolutePath());
            }
        }

    }

    /**
     * method to read through the INTERLIS file
     */
    @FXML
    private void readInterlisFile() {
        String path = txtInterlis.getText();
        //checks if the path is null
        if (!(path.equals(""))) {
            this.lblStatus.setText("Daten werden eingelesen");
            this.showLoadingStatus();

            // new task thread which reads the INTERLIS-file
            Task task = new Task<Void>() {
                @Override
                public Void call() {
                    try {
                        readInterlis.readData(path);
                        readInterlis.analyze();
                        polyMap = readInterlis.getPolyMap();
                        polyWArcMap = readInterlis.getPolyWArcMap();
                        timeline.stop();
                        return null;

                    } catch (IoxException ex) {
                        ex.printStackTrace();
                    }
                    return null;
                }
            };
            new Thread(task).start();
            task.setOnSucceeded(e -> {
                this.lblStatus.setText("Daten wurden erfolgreich eingelesen.");
            });
        } else {
            this.lblStatus.setText("Geben Sie den Pfad zu einer INTERLIS-Datei an.");

        }

    }

    /**
     * Starts the tests for the geometry objects checks if all requirements are
     * achieved
     */
    @FXML
    private void startTests() {

        //checks if all needed information is given for the tests
        if (!(this.polyMap.isEmpty() && this.polyWArcMap.isEmpty())) {

            if (!(this.txtLog.getText().equals(""))) {

                if (!(this.txtNumTestObjects.getText().equals(""))) {

                    if (!((this.txtCoordsX.getText().length() != 0 && this.txtCoordsY.getText().isEmpty()) || (this.txtCoordsY.getText().length() != 0 && this.txtCoordsX.getText().isEmpty()))) {

                        if (this.chkBoxLine.isSelected() || this.chkBoxPoly.isSelected() || this.chkBoxArc.isSelected()) {
                            Logger.setup(this.txtLog.getText());
                            LOGGER.info("Start geometry Tests");
                            LOGGER.log(Level.INFO, "INTERLIS-file: {0}", this.txtInterlis.getText());
                            LOGGER.log(Level.INFO, "Log-file path: {0}", this.txtLog.getText());
                            LOGGER.log(Level.INFO, "Map-file path: {0}", this.txtMap.getText());
                            LOGGER.log(Level.INFO, "Line geometries: {0}", this.chkBoxLine.isSelected());
                            LOGGER.log(Level.INFO, "Polygon geometries: {0}", this.chkBoxPoly.isSelected());
                            LOGGER.log(Level.INFO, "Polygon with Arc geometries: {0}", this.chkBoxArc.isSelected());
                            LOGGER.log(Level.INFO, "NumTestObjects: {0}", this.txtNumTestObjects.getText());
                            LOGGER.log(Level.INFO, "X : {0}", this.txtCoordsX.getText());
                            LOGGER.log(Level.INFO, "Y : {0}", this.txtCoordsY.getText());

                            //values are set
                            GeometryTests results = new GeometryTests(this.chkBoxLine.isSelected(),
                                    this.chkBoxPoly.isSelected(),
                                    this.chkBoxArc.isSelected(),
                                    this.txtNumTestObjects.getText(),
                                    this.txtCoordsX.getText(),
                                    this.txtCoordsY.getText(),
                                    this.polyMap,
                                    this.polyWArcMap);

                            this.lblStatus.setText("Tests werden durchgeführt");
                            this.showLoadingStatus();

                            Task task = new Task<Void>() {
                                @Override
                                public Void call() {
                                    results.start();
                                    mainApp.loadTableView(results);
                                    timeline.stop();
                                    return null;
                                }
                            };
                            new Thread(task).start();
                            task.setOnSucceeded(e -> {
                                this.lblStatus.setText("Tests wurden durchgeführt.");
                            });

                        } else {
                            this.lblStatus.setText("Eine Geometrie muss noch ausgewählt werden.");
                        }
                    } else {
                        this.lblStatus.setText("Es müssen beide Koordinaten angegeben werden.");
                    }
                } else {
                    this.lblStatus.setText("Bitte geben Sie die Anzahl TestObjekte an.");
                }
            } else {
                this.lblStatus.setText("Bitte geben Sie ein Verzeichnis für die Log-Datei an");
            }
        } else {
            this.lblStatus.setText("Bitte lesen Sie eine INTERLIS Datei ein.");
        }
    }

    /**
     * gathers all required variables and creates the geojson file
     */
    @FXML
    private void createGeoJson() {
        if (!(this.polyMap.isEmpty() && this.polyWArcMap.isEmpty())) {

            if (!(this.txtMap.getText().equals(""))) {
                Map<String, IomObject> polygons = new HashMap<>();
                Data2GeoJSON data2GeoJson = new Data2GeoJSON();

                this.lblStatus.setText("GeoJSON-Datei wird erstellt");
                this.showLoadingStatus();

                Task task = new Task<Void>() {
                    @Override
                    public Void call() {
                        //all polygons in one map
                        polygons.putAll(polyMap);
                        polygons.putAll(polyWArcMap);
                        data2GeoJson.createGeoJSON(txtMap.getText(), polygons, readInterlis.getInfoMap());
                        timeline.stop();
                        return null;
                    }
                };
                new Thread(task).start();
                task.setOnSucceeded(e -> {
                    this.lblStatus.setText("GeoJSON-Datei erstellt.");
                });

            } else {
                this.lblStatus.setText("Speicherort Map angeben");
            }
        } else {
            this.lblStatus.setText("Bitte lesen Sie eine INTERLIS Datei ein.");
        }
    }

    /**
     * loads the property file and sets the themes to search through the
     * INTERLIS file
     */
    public void loadProperties() {
        try {
            Properties prop = new Properties();
            String propPath = System.getProperty("user.dir") + this.SLASH + "properties";
            String decodedPath = URLDecoder.decode(propPath, "UTF-8");
            InputStream input = new FileInputStream(decodedPath + this.SLASH + "ipa.properties");
            prop.load(input);
            for (Entry<Object, Object> e : prop.entrySet()) {
                this.readInterlis.setTheme(e);
            }

            input.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * sets the mainApp to load and switch the view
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    /**
     * method to display the loading of a process
     */
    public void showLoadingStatus() {
        final String status1 = lblStatus.getText() + " . . .";
        final String status2 = lblStatus.getText() + " .";
        timeline = new Timeline(
                new KeyFrame(Duration.ZERO, new EventHandler() {
                    @Override
                    public void handle(Event event) {
                        String statusText = lblStatus.getText();
                        lblStatus.setText(
                                ((status1).equals(statusText))
                                ? status2
                                : statusText + " ."
                        );
                    }
                }),
                new KeyFrame(Duration.millis(1000))
        );
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }
}
